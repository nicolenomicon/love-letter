import java.util.ArrayList;
import java.util.Collections;

public class Deck
{
    private ArrayList<Card> cards;
    private Card missingCard;
    //private int deckSize;

    public Deck()
    {
        cards = new ArrayList<>(16);
        
        //Adds the correct number of each card to the list
        for (int x = 0;x < 16;x++)
        {
            if (x < 5)
            {
                cards.add(new Card(1));
            }
            else if (x < 7)
            {
                cards.add(new Card(2));
            }
            else if (x < 9)
            {
                cards.add(new Card(3));
            }
            else if (x < 11)
            {
                cards.add(new Card(4));
            }
            else if (x < 13)
            {
                cards.add(new Card(5));
            }
            else if (x < 14)
            {
                cards.add(new Card(6));
            }
            else if (x < 15)
            {
                cards.add(new Card(7));
            }
            else if (x < 16)
            {
                cards.add(new Card(8));
            }
        }
        
        //Shuffles the deck and removes the first card
        Collections.shuffle(cards);
        missingCard = cards.get(0);
        //deckSize = 15;
        cards.remove(0);
        cards.trimToSize();
    }

    //Removes a card from the deck and returns it
    public Card getCard(int cardNo)
    {
    	Card drawnCard = cards.get(cardNo);
    	cards.remove(cardNo);
    	cards.trimToSize();
    	return drawnCard;
    }
    
    public Card getMissingCard()
    {
    	return missingCard;
    }
    
    //Gets the current size of the deck
    public int getSize()
    {
    	return cards.size();
    }
}
