import java.util.Scanner;

public class Main
{
	static Scanner input = new Scanner(System.in);
	public static void main(String[] args)
	{
		
		int numberOfPlayers = 0;
		boolean validPlay = false;
		//Gets the number of players for this game
		while (numberOfPlayers < 2 || numberOfPlayers > 4)
		{
			System.out.println("Enter number of players between 2 and 4: ");
			while (!input.hasNextInt())
				input.next();

			numberOfPlayers = input.nextInt();
			//input.next();
			//System.out.println(numberOfPlayers);
		}

		Deck deck = new Deck();
		Player[] players = new Player[numberOfPlayers];
		Card playedCard;

		//Creates each of the players and gives them their first card
		for (int x = 0;x < numberOfPlayers;x++)
		{
			players[x] = new Player(x+1);
			players[x].setCard(deck.getCard(x));
			System.out.println("Press enter to reveal Player " + players[x].getId() + "'s card...");
			
			
			while(!input.nextLine().equals(""));
			
			System.out.println("Player " + players[x].getId() + ", your first card is: ");
			System.out.println(players[x].getCard(1));
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
			System.out.println("Press enter to continue to the next player...");
			
			while(!input.nextLine().equals(""));
		}
		
		//Counter for who's turn it is
		int turnCounter = 0;
		
		while (deck.getSize() != 0)
		{
			//Draws a card and prints what it is
			System.out.println("Player " + players[turnCounter].getId() + ", you have drawn a: ");
			players[turnCounter].draw(deck.getCard(0));
			System.out.println(players[turnCounter].getCard(2).toString());
			
			//Prints the player's hand
			System.out.println("Your hand is: ");
			System.out.println(players[turnCounter].getCard(1));
			System.out.println(players[turnCounter].getCard(2));
			
			//Selection for play action
			int playSelection = 0;
			while (playSelection < 1 || playSelection > 3 || validPlay == false)
			{
				//Prints the menu
				System.out.println("Select an option:\n1. Play " 
						+ players[turnCounter].getCard(1) + "\n2. Play " 
						+ players[turnCounter].getCard(2) + "\n3. Get card descriptions");
				
				//Gets the player's selection
				while (!input.hasNextInt())
					input.next();
				
				playSelection = input.nextInt();
				
				//Plays the first or second card or gets card descriptions
				if (playSelection == 1 || playSelection == 2)
				{
					playedCard = players[turnCounter].getCard(playSelection);
					validPlay = cardEffect(playedCard, players, deck, turnCounter);
				}
				else if (playSelection == 3)
				{
					furtherDetails(turnCounter, players);			
					playSelection = 0;
				}
			}
			
			//Increments the turn counter or resets it if a full round of turns have been played
			if (players[turnCounter].getId() == players.length)
			{
				turnCounter = 0;
			}
			else
			{
				turnCounter++;
			}
		}
		
		input.close();
	}
	
	//Prints the names, descriptions, and ranks of the cards in a player's hand
	public static void furtherDetails(int turnCounter, Player[] players)
	{
		String[] firstCardDetails = players[turnCounter].getCard(1).toStringArray();
		String[] secondCardDetails = players[turnCounter].getCard(2).toStringArray();
		System.out.println("Player " + turnCounter + "'s " + "hand:\n"
				+ "Card 1: " + firstCardDetails[0] + ": " + firstCardDetails[1] + " (" + firstCardDetails[2] + ")" + "\n"
				+ "Card 2: " + secondCardDetails[0] + ": " + secondCardDetails[1] + " (" + secondCardDetails[2] + ")" + "\n");
	}

	public static boolean cardEffect(Card playedCard, Player players[], Deck deck, int currentPlayer)
	{
		int targetNo = 0; 
		int targetSelection = 0;
		
		switch(playedCard.getRank())
		{
			case 1:
				System.out.println("Select a player to target with your guard: \n");
				
				if (currentPlayer != 0 && players[0].getTargettable())
				{
					targetNo++;
					System.out.println(Integer.toString(targetNo) + ": Player 1\n");
				}
				
				if (currentPlayer != 1 && players[1].getTargettable())
				{
					targetNo++;
					System.out.println(Integer.toString(targetNo) + ": Player 2\n");
				}
				
				if (players.length > 2)
				{
					if (currentPlayer != 2 && players[2].getTargettable())
					{
						targetNo++;
						System.out.println(Integer.toString(targetNo) + ": Player 3\n");
					}
				}
				
				if(players.length > 3)
				{
					if (currentPlayer != 3 && players[3].getTargettable())
					{
						targetNo++;
						System.out.println(Integer.toString(targetNo) + ": Player 4\n");
					}
				}
				
				Player[] targets = new Player[targetNo];
				
				while(targetSelection > 0 || targetSelection < targetNo)
				{
					while (!input.hasNextInt())
						input.next();
					
					targetSelection = input.nextInt();
				}
				
				if(targetNo == 0)
				{
					return false;
				}
				break;
				
			case 2:
				
				break;
				
			case 3:
				
				break;
				
			case 4:
				
				break;
				
			case 5:
				
				break;
				
			case 6:
				
				break;
				
			case 7:
				
				break;
				
			case 8:
				
				break;
		}
		
		return true;
	}
}
