public class Player
{
    private int id;
    private Card card;
    private Card nextCard;
    private Card playedCard;
    private boolean targettable;

    public Player(int idIn)
    {
        id = idIn;
        targettable = true;
    }

    //For setting the initial card
    public void setCard(Card newCard)
    {
        card = newCard;
    }

    //Draws a new card
    public void draw(Card newCard)
    {
        nextCard = newCard;
    }

    //When a card is played, if it's the first card, move the new card to the old cards position,
    //otherwise make the second card null after it is played
    public Card play(int selection)
    {
        if (selection == 1)
        {
            playedCard = card;
            card = nextCard;
            nextCard = null;
            return playedCard;
        }
        else
        {
            playedCard = nextCard;
            nextCard = null;
            return playedCard;
        }
    }
    
    public int getId()
    {
    	return id;
    }
    
    //Gets a card from the player's hand
    public Card getCard(int selection)
    {
    	if(selection == 1)
    	{
    		return card;
    	}
    	else
    	{
    		return nextCard;
    	}
    }
    
    public boolean getTargettable()
    {
    	return targettable;
    }
}
