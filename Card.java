public class Card
{
    private int rank;
    private String[] names = {"Guard", "Priest", "Baron", "Handmaid", "Prince", "King", "Countess", "Princess"};
    private String[] descriptions = {"Guess a player's identity to knock them out of the round.",
    								 "Look at another player's hand.",
    								 "Compare hands with another player. Lower value is knocked out.",
    								 "Cannot be targeted until your net turn",
    								 "Force another player to discard their card and draw a new one",
    								 "Swap hands with another player",
    								 "Must discard if other card is King or Prince",
    								 "Lose if discarded"};
    private String name, description;

    //Constructs the card based on the input rank
    public Card(int rankIn)
    {
        rank = rankIn;
        name = names[rankIn - 1];
        description = descriptions[rankIn - 1];
    }
    
    //Returns the card's name
    public String toString()
    {
    	return name;
    }
    
    //Returns all the card's details
    public String[] toStringArray()
    {
    	String[] details = new String[] {name, description, Integer.toString(rank)};
    	return details;
    }
    
    public int getRank()
    {
    	return rank;
    }
}
